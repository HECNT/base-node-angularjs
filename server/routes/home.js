var express = require('express');
var router = express.Router();
var Home = require('../controllers/home');
var formidable = require('formidable');
var fs = require('fs');

// METHOD'S
// GET
router.post('/file', uploadFile);


function uploadFile (req, res) {
  var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
      var oldpath = files.filetoupload.path;
      var newpath = '/home/desarollo2/' + files.filetoupload.name;
      fs.rename(oldpath, newpath, function (err) {
        if (err) throw err;
        res.render('upload');
      });
    });
}


module.exports = router;
